package org.springframework.samples.petclinic.util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.repository.test.UserRepositoryStubImpl;
import org.springframework.samples.petclinic.service.UserService;
import org.springframework.samples.petclinic.service.UserServiceImpl;

@Configuration
@ComponentScan(value={"org.springframework.samples.petclinic.service.UserServiceImpl"})
@Profile("test")
public class UserConfig {
    @Bean
    public UserService userService() {
        return new UserServiceImpl();
    }

    @Bean
    public UserRepository userRepository() {
        return new UserRepositoryStubImpl();
    }
}
