package org.springframework.samples.petclinic.repository.test;

import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
@Profile("test")
public class UserRepositoryStubImpl implements UserRepository {

    private ArrayList<User> users = new ArrayList<>();

    public void save(User user) throws DataAccessException {
        this.users.add(user);
    }

    public int getUsersSize() {
        return users.size();
    }

}
