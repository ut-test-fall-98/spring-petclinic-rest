package org.springframework.samples.petclinic.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.samples.petclinic.model.Specialty;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SpecialtyRestController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets/update-specialty")
public class SpecialtyRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ClinicService clinicService;

    @Captor
    ArgumentCaptor<Specialty> argCaptor;

    private final int requestObjectId = 1;

    @Test
    public void tryToSetSpecialtyWithoutAdminRoleCauseError() throws Exception {
        this.mockMvc
            .perform(put("/api/specialties/{specialtyId}", requestObjectId)
                .with(csrf()))
            .andDo(print())
            .andExpect(status().isUnauthorized())
            .andDo(document("call-without-admin-authority"));
    }

    @Test
    @WithMockUser(roles = "VET_ADMIN")
    public void setSpecialtyNameWithNotFoundedIdShouldRespondNotFound() throws Exception {
        when(clinicService.findSpecialtyById(requestObjectId)).thenReturn(null);
        Specialty requestObject = new Specialty();

        requestObject.setId(requestObjectId);
        requestObject.setName("request");

        this.mockMvc
            .perform(put("/api/specialties/{specialtyId}", requestObjectId)
                .with(csrf())
                .content(objectMapper.writeValueAsString(requestObject))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
            )
            .andDo(print())
            .andExpect(status().isNotFound())
            .andDo(document("request-for-updating-not-found-specialty"));
    }

    @Test
    @WithMockUser(roles = "VET_ADMIN")
    public void setSpecialtyWithMalformedObjectInRequestShouldRespondBadRequest() throws Exception {
        Specialty requestObject = new Specialty();
        this.mockMvc
            .perform(
                put("/api/specialties/{specialtyId}", requestObjectId)
                    .with(csrf())
                    .content(objectMapper.writeValueAsString(requestObject) + "a")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
            )
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andDo(document("call-with-malformed-specialty-object"));
    }

    @Test
    @WithMockUser(roles = "VET_ADMIN")
    public void setSpecialtyWithEmptyObjectInRequestShouldRespondBadRequest() throws Exception {
        this.mockMvc
            .perform(
                put("/api/specialties/{specialtyId}", requestObjectId)
                    .with(csrf())
                    .content("")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
            )
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andDo(document("call-with-empty-request-object"));
    }

    @Test
    @WithMockUser(roles = "VET_ADMIN")
    public void setSpecialtyNameWithExistsIdShouldRespondOkAndSaveNewSpecialty() throws Exception {
        Specialty requestObject = new Specialty();
        requestObject.setName("new name");
        requestObject.setId(requestObjectId);

        Specialty oldSpecialty = new Specialty();
        oldSpecialty.setId(requestObjectId);
        when(clinicService.findSpecialtyById(requestObjectId)).thenReturn(oldSpecialty);
        this.mockMvc
            .perform(
                put("/api/specialties/{specialtyId}", requestObjectId)
                    .with(csrf())
                    .content(objectMapper.writeValueAsString(requestObject))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
            )
            .andDo(print())
            .andExpect(status().isNoContent())
            .andDo(document("update-specialty-successfully"));

        verify(clinicService, times(1)).findSpecialtyById(requestObject.getId());
        verify(clinicService).saveSpecialty(argCaptor.capture());
        assert (requestObject.getName().equals(argCaptor.getValue().getName()));
        verify(clinicService, times(1)).saveSpecialty(oldSpecialty);
    }
}
