package org.springframework.samples.petclinic.rest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.samples.petclinic.model.Specialty;
import org.springframework.samples.petclinic.model.Vet;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BindingResult;
import static org.mockito.ArgumentMatchers.*;
import java.util.ArrayList;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class VetRestControllerUpdateVetTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    BindingResult bindingResult;

    @Mock
    Vet vet;

    @Mock
    ClinicService clinicService;

    @InjectMocks
    VetRestController vetRestController;

    private int vetId = 5;

    // [0,1]
    // [0,2,3,4]
    // [0,2,3,5,8,7,6,8,9]

    @Test
    public void Test1() {
        // [0,1] with first condition in if
        when(bindingResult.hasErrors()).thenReturn(true);

        vetRestController.updateVet(vetId, vet, bindingResult);
    }

    @Test
    public void Test2() {
        // [0,1] with second condition in if
        when(bindingResult.hasErrors()).thenReturn(false);

        vetRestController.updateVet(vetId, null, bindingResult);
    }

    @Test
    public void Test3() {
        // [0,2,3,4]
        when(bindingResult.hasErrors()).thenReturn(false);
        when(clinicService.findVetById(any(Integer.class))).thenReturn(null);

        vetRestController.updateVet(vetId, vet, bindingResult);
    }

    @Test
    public void Test4() {
        // [0,2,3,5,8,7,6,8,9]
        ArrayList<Specialty> specialties = new ArrayList<>();
        specialties.add(new Specialty());
        when(vet.getSpecialties()).thenReturn(specialties);
        when(bindingResult.hasErrors()).thenReturn(false);
        when(clinicService.findVetById(any(Integer.class))).thenReturn(new Vet());

        vetRestController.updateVet(vetId, vet, bindingResult);
    }

}
