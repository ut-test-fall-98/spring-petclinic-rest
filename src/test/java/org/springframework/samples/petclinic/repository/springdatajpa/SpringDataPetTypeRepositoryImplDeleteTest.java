package org.springframework.samples.petclinic.repository.springdatajpa;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.samples.petclinic.model.Pet;
import org.springframework.samples.petclinic.model.PetType;
import org.springframework.samples.petclinic.model.Visit;
import org.springframework.test.context.junit4.SpringRunner;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.*;


@RunWith(SpringRunner.class)
public class SpringDataPetTypeRepositoryImplDeleteTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    EntityManager em;

    @Mock
    Pet pet;

    @Mock
    Query query;

    @InjectMocks
    SpringDataPetTypeRepositoryImpl springDataPetTypeRepository;

    // [0,1,2,3,4,5,6,4,7,2,8]

    @Test
    public void Test1() {

        List<Pet> pets = new ArrayList<>();
        List<Visit> visits = new ArrayList<>();
        visits.add(new Visit());
        pets.add(pet);
        when(pet.getVisits()).thenReturn(visits);
        when(em.createQuery(any(String.class))).thenReturn(query);
        when(em.contains(any(PetType.class))).thenReturn(true);
        when(query.getResultList()).thenReturn(pets);

        springDataPetTypeRepository.delete(new PetType());
    }

    @Test
    public void Test2() {
        List<Pet> pets = new ArrayList<>();
        List<Visit> visits = new ArrayList<>();
        visits.add(new Visit());
        pets.add(pet);
        when(pet.getVisits()).thenReturn(visits);
        when(em.createQuery(any(String.class))).thenReturn(query);
        when(query.getResultList()).thenReturn(pets);
        when(em.contains(any(PetType.class))).thenReturn(false);

        springDataPetTypeRepository.delete(new PetType());
    }
}
