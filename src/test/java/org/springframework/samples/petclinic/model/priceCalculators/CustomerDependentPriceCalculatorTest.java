package org.springframework.samples.petclinic.model.priceCalculators;

import java.util.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;
import org.springframework.samples.petclinic.model.Pet;
import org.springframework.samples.petclinic.model.PetType;
import org.springframework.samples.petclinic.model.UserType;
import org.springframework.samples.petclinic.service.clinicService.ApplicationTestConfig;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
@ContextConfiguration(classes = ApplicationTestConfig.class)
public class CustomerDependentPriceCalculatorTest {

    private PriceCalculator priceCalculator;
    private PetType rarePetType;
    private PetType usualPetType;

    @Before
    public void setup() {
        priceCalculator = new CustomerDependentPriceCalculator();
        rarePetType = Mockito.mock(PetType.class);
        usualPetType = Mockito.mock(PetType.class);

        when(rarePetType.getRare()).thenReturn(true);
        when(usualPetType.getRare()).thenReturn(false);
    }

    @Test
    public void onePetRareOnePetUnRare() {
        Pet rarePet = new Pet();
        rarePet.setType(rarePetType);

        Pet unRarePet = new Pet();
        unRarePet.setType(usualPetType);

        ArrayList<Pet> pets = new ArrayList<>();
        pets.add(rarePet);
        pets.add(unRarePet);
        pets.add(rarePet);
        pets.add(rarePet);

        assertEquals(priceCalculator.calcPrice(pets, 10, 20, UserType.NEW), 124.8, 0.0001);
        assertEquals(priceCalculator.calcPrice(pets, 10, 20, UserType.GOLD), 109.84, 0.0001);
    }

    @Test
    public void minimumInfantRare() { // (RARE USUAL) (GOLD NEW) (AGE 10 years ago)
        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.FEBRUARY, 1);
        Date birthDate = cal.getTime();

        Pet rarePet = new Pet();
        rarePet.setType(rarePetType);
        rarePet.setBirthDate(new Date());

        Pet unRarePet = new Pet();
        unRarePet.setType(usualPetType);
        unRarePet.setBirthDate(birthDate);

        ArrayList<Pet> pets = new ArrayList<>();

        for (int i = 0; i < 8; i++) {
            pets.add(unRarePet);
        }
        pets.add(rarePet);
        pets.add(rarePet);

        assertEquals(priceCalculator.calcPrice(pets, 20, 30, UserType.GOLD), 327.04, 0.0001);
    }

    @Test
    public void DiscountBoundWith10UnRare() {
        Date birthDate = new Date();
        birthDate.setTime(1900);

        Pet rarePet = new Pet();
        rarePet.setType(rarePetType);
        rarePet.setBirthDate(birthDate);

        Pet unRarePet = new Pet();
        unRarePet.setType(usualPetType);
        unRarePet.setBirthDate(birthDate);

        ArrayList<Pet> pets = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            pets.add(rarePet);
        }

        assertEquals(priceCalculator.calcPrice(pets, 20, 30, UserType.GOLD), 304, 0.0001);
        assertEquals(priceCalculator.calcPrice(pets, 20, 30, UserType.NEW), 362, 0.0001);
    }

    @Test
    public void DiscountBoundWith4Rare2UnRare() {
        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.FEBRUARY, 1);
        Date birthDate = cal.getTime();

        Pet rarePet = new Pet();
        rarePet.setType(rarePetType);
        rarePet.setBirthDate(birthDate);

        Pet unRarePet = new Pet();
        unRarePet.setType(usualPetType);
        unRarePet.setBirthDate(birthDate);

        ArrayList<Pet> pets = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            pets.add(rarePet);
        }
        pets.add(unRarePet);
        pets.add(unRarePet);

        assertEquals(priceCalculator.calcPrice(pets, 20, 30, UserType.GOLD), 234.88, 0.0001);
        assertEquals(priceCalculator.calcPrice(pets, 20, 30, UserType.NEW), 279.92, 0.0001);
    }
}
