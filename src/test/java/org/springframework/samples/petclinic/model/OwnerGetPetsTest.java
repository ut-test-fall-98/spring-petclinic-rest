package org.springframework.samples.petclinic.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class OwnerGetPetsTest {

    @Parameterized.Parameters
    public static String[][][] data() {
        return new String[][][]{
            {{"petname1", "petname2", "petname3", "petname4"}},
            {{"petname1", "petname2", "petname4", "petname3"}},
            {{"petname1", "petname3", "petname4", "petname2"}},
            {{"petname1", "petname3", "petname2", "petname4"}},
            {{"petname2", "petname1", "petname3", "petname4"}},
            {{"petname2", "petname1", "petname4", "petname3"}},
            {{"petname1", "petname1", "petname4", "petname3"}},
        };
    }

    private Owner owner;
    private String[] nameList;

    public OwnerGetPetsTest(String[] names) {
        this.owner = new Owner();
        this.nameList = names;
        for (String name : names) {
            Pet newPet = new Pet();
            newPet.setName(name);
            this.owner.addPet(newPet);
        }
    }

    @Test
    public void testGetPets() {
        List<Pet> receivedPetList = this.owner.getPets();
        Arrays.sort(this.nameList);
        assertEquals(receivedPetList.stream().map(Pet::getName).collect(Collectors.toList()),
            Arrays.asList(this.nameList));
    }
}
