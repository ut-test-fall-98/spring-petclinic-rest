package org.springframework.samples.petclinic.model;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Test class for {@link Owner}
 *
 * @author Turambar
 */
@RunWith(JUnit4.class)
public class OwnerAccessMethodsTests {

    private Owner owner;

    @Before
    public void setup() {
        this.owner = new Owner();
    }

    @After
    public void tearDown() {
        this.owner = null;
    }

    @Test
    public void addressAccessTest() {
        String sampleAddress = "sample address";
        owner.setAddress(sampleAddress);
        assertEquals(sampleAddress, owner.getAddress());

        owner.setAddress(sampleAddress + "v2");
        assertEquals(sampleAddress + "v2", owner.getAddress());
    }

    @Test
    public void cityAccessTest() {
        String sampleCity = "sample city";
        owner.setCity(sampleCity);
        assertEquals(sampleCity, owner.getCity());

        owner.setCity(sampleCity + "v2");
        assertEquals(sampleCity + "v2", owner.getCity());
    }


    @Test
    public void telephoneAccessTest() {
        String sampleTelephone = "0939923781";
        owner.setTelephone(sampleTelephone);
        assertEquals(sampleTelephone, owner.getTelephone());

        owner.setTelephone(sampleTelephone + "v2");
        assertEquals(sampleTelephone + "v2", owner.getTelephone());
    }

    @Test
    public void checkOwnerDefaultConstruction() {
        assertNull(owner.getAddress());
        assertNull(owner.getCity());
        assertNull(owner.getTelephone());
    }
}

