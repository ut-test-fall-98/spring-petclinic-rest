package org.springframework.samples.petclinic.model;

import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import static junit.framework.TestCase.assertEquals;

@RunWith(Theories.class)
public class OwnerGetPetTests {

    @DataPoints
    public static String[] actualPetNamesDataPoints() {
        return new String[]{"name1", "name2", "Name1", "Name2"};
    }

    @DataPoints
    public static boolean[] ignoreNewDataPoints() {
        return new boolean[]{true, false};
    }

    @DataPoints
    public static boolean[] isNewDataPoints() {
        return new boolean[]{true, false};
    }

    @DataPoints
    public static String[] lookedUpNameDataPoints() {
        return new String[]{"name1", "name2", "Name1", "Name2"};
    }

    @Theory
    public void testGetPet(String actualPetName,
                           boolean ignoreNew,
                           boolean isNew,
                           String lookedUpName) {
        Owner owner = new Owner();
        Pet newPet = new Pet();
        newPet.setOwner(owner);
        newPet.setId(isNew ? null : 1);
        newPet.setName(actualPetName);
        owner.addPet(newPet);
        assertEquals(owner.getPet(lookedUpName, ignoreNew) != null,
            (!ignoreNew || !isNew) && actualPetName.toLowerCase().equals(lookedUpName.toLowerCase()));
    }
}
