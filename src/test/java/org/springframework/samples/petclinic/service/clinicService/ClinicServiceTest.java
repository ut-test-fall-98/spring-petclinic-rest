package org.springframework.samples.petclinic.service.clinicService;

import com.github.mryf323.tractatus.ClauseCoverage;
import com.github.mryf323.tractatus.Valuation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.samples.petclinic.repository.*;
import org.springframework.samples.petclinic.service.ClinicServiceImpl;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.github.mryf323.tractatus.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.samples.petclinic.model.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(SpringJUnit4ClassRunner.class)
public class ClinicServiceTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Mock
    PetRepository petRepository;

    @Mock
    VetRepository vetRepository;

    @Mock
    VisitRepository visitRepository;

    @InjectMocks
    ClinicServiceImpl clinicService;

    @Mock
    Specialty specialty;

    private Collection<Pet> pets;
    private Collection<Vet> vets;

    @Before
    public void setup() {
        pets = new ArrayList<>();
        vets = new ArrayList<>();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    // a: pets.size() > 0
    @NearFalsePoint(
        predicate = "a",
        cnf = "a",
        clause = 'a',
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )

    // a: notVisited.size() > 0
    @NearFalsePoint(
        predicate = "a",
        cnf = "a",
        clause = 'a',
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )

    // a: last.isPresent() == true
    @NearFalsePoint(
        predicate = "a",
        cnf = "a",
        clause = 'a',
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )

    // a: last.isPresent() == true
    @CACC(
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }, predicateValue = false
    )

    // a: pets.size() > 0
    @CACC(
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        },
        predicateValue = false
    )

    // a: notVisited.size() > 0
    @CACC(
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        },
        predicateValue = false
    )

    // a: i < pets.size()
    @ClauseCoverage(
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )

    // a: last.isPresent() == true
    @ClauseCoverage(
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )

    // a: notVisited.size() > 0
    @ClauseCoverage(
        predicate = "b",
        valuations = {
            @Valuation(clause = 'b', valuation = false)
        }
    )

    @Test
    public void visitOwnerPetsNoEnterLoop() {
        Owner owner = new Owner();

        when(petRepository.findByOwner(owner)).thenReturn(pets);

        clinicService.visitOwnerPets(owner);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    // a: len(pets) > 0
    @UniqueTruePoint(
        predicate = "a",
        cnf = "a",
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )

    // a: last.isPresent() == true
    @UniqueTruePoint(
        predicate = "a",
        cnf = "a",
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )

    // a: last.isPresent() == true
    @CACC(
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }, predicateValue = true
    )

    // a: pets.size() > 0
    @CACC(
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        },
        predicateValue = true
    )

    // a: age > 3
    // b: daysFromLastVisit > 364
    // c: age <= 3
    // d: daysFromLastVisit > 182
    @ClauseCoverage(
        predicate = "(a && b) || (c && d)",
        valuations = {
            @Valuation(clause = 'a', valuation = true),
            @Valuation(clause = 'b', valuation = true),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = true)
        }
    )

    // a: last.isPresent()
    @ClauseCoverage(
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )

    // a: i < pets.size()
    @ClauseCoverage(
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )

    @Test
    public void visitOwnerPetsEnterLoopWithFirstConditionalTrueAndSecondConditionalTrue() {
        Pet pet = Mockito.mock(Pet.class);

        PetType petType = new PetType();
        petType.setName("mamal");

        java.util.Date birthDate = java.sql.Date.valueOf(LocalDate.now().minusYears(4));

        Owner owner = new Owner();

        Visit last = new Visit();
        last.setDate(birthDate);

        pets.add(pet);

        Vet vet = new Vet();
        vet.addSpecialty(specialty);

        vets.add(vet);

        when(pet.getType()).thenReturn(petType);
        when(pet.getBirthDate()).thenReturn(birthDate);
        when(pet.getLastVisit()).thenReturn(Optional.of(last));
        when(specialty.canCure(petType)).thenReturn(true);
        when(vetRepository.findAll()).thenReturn(vets);
        when(petRepository.findByOwner(owner)).thenReturn(pets);

        clinicService.visitOwnerPets(owner);
        Mockito.verify(visitRepository).save(any(Visit.class));
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    // a: notVisited.size() > 0
    @UniqueTruePoint(
        predicate = "a",
        cnf = "a",
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )

    // a: notVisited.size() > 0
    @CACC(
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        },
        predicateValue = true
    )

    // a: notVisited.size() > 0
    @ClauseCoverage(
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )

    @Test
    public void visitOwnerPetsEnterLoopWithFirstConditionalFalseAndLastConditionalTrue() {
        Owner owner = new Owner();

        Pet pet = Mockito.mock(Pet.class);

        PetType petType = Mockito.mock(PetType.class);

        pets.add(pet);

        Vet vet = new Vet();
        vets.add(vet);

        vet.addSpecialty(specialty);

        when(pet.getType()).thenReturn(petType);
        when(pet.getLastVisit()).thenReturn(Optional.empty());
        when(petRepository.findByOwner(owner)).thenReturn(pets);
        when(vetRepository.findAll()).thenReturn(vets);
        when(specialty.canCure(petType)).thenReturn(false);

        expectedException.expect(ClinicServiceImpl.VisitException.class);

        clinicService.visitOwnerPets(owner);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    // a: age > 3
    // b: daysFromLastVisit > 364
    // c: age <= 3
    // d: daysFromLastVisit > 182
    @NearFalsePoint(
        predicate = "(a && b) || (c && d)",
        cnf = "(a && b) || (c && d)",
        implicant = "cd",
        clause = 'd',
        valuations = {
            @Valuation(clause = 'a', valuation = false),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = false)
        }
    )

    // a: age > 3
    // b: daysFromLastVisit > 364
    // c: age <= 3
    // d: daysFromLastVisit > 182
    @CACC(
        predicate = "(a && b) || (c && d)",
        majorClause = 'd',
        valuations = {
            @Valuation(clause = 'a', valuation = false),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = false)
        },
        predicateValue = false
    )

    // a: age > 3
    // b: daysFromLastVisit > 364
    // c: age <= 3
    // d: daysFromLastVisit > 182
    @ClauseCoverage(
        predicate = "(a && b) || (c && d)",
        valuations = {
            @Valuation(clause = 'a', valuation = false),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = false)
        }
    )

    @Test
    public void visitOwnerPetsEnterLoopWithFirstConditionalTrueAndSecondConditionalFalseByAllClauses() {
        Pet pet = Mockito.mock(Pet.class);

        PetType petType = new PetType();
        petType.setName("mamal");

        pets.add(pet);

        Vet vet = new Vet();
        vet.addSpecialty(specialty);

        vets.add(vet);

        Owner owner = new Owner();
        Visit last = new Visit();
        java.util.Date birthDate = java.sql.Date.valueOf(LocalDate.now().minusDays(1));
        last.setDate(birthDate);

        when(pet.getLastVisit()).thenReturn(Optional.of(last));
        when(pet.getBirthDate()).thenReturn(birthDate);
        when(pet.getType()).thenReturn(petType);
        when(petRepository.findByOwner(owner)).thenReturn(pets);
        when(vetRepository.findAll()).thenReturn(vets);
        when(specialty.canCure(petType)).thenReturn(true);

        clinicService.visitOwnerPets(owner);
        Mockito.verify(visitRepository, never()).save(any(Visit.class));
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    // a: age > 3
    // b: daysFromLastVisit > 364
    // c: age <= 3
    // d: daysFromLastVisit > 182
    @NearFalsePoint(
        predicate = "(a && b) || (c && d)",
        cnf = "(a && b) || (c && d)",
        implicant = "cd",
        clause = 'b',
        valuations = {
            @Valuation(clause = 'a', valuation = true),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = false)
        }
    )

    // a: age > 3
    // b: daysFromLastVisit > 364
    // c: age <= 3
    // d: daysFromLastVisit > 182
    @CACC(
        predicate = "(a && b) || (c && d)",
        majorClause = 'b',
        valuations = {
            @Valuation(clause = 'a', valuation = true),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = false)
        }, predicateValue = false
    )

    @Test
    public void visitOwnerPetsEnterLoopWithFirstConditionalTrueAndSecondConditionalFalse() {
        Pet pet = Mockito.mock(Pet.class);

        PetType petType = new PetType();
        petType.setName("mamal");

        java.util.Date birthDate = java.sql.Date.valueOf(LocalDate.now().minusYears(4));
        java.util.Date visitDate = java.sql.Date.valueOf(LocalDate.now().minusDays(1));

        pets.add(pet);

        Owner owner = new Owner();
        Visit last = new Visit();
        last.setDate(visitDate);

        Vet vet = new Vet();
        vet.addSpecialty(specialty);
        vets.add(vet);

        when(pet.getLastVisit()).thenReturn(Optional.of(last));
        when(pet.getType()).thenReturn(petType);
        when(pet.getBirthDate()).thenReturn(birthDate);
        when(petRepository.findByOwner(owner)).thenReturn(pets);
        when(vetRepository.findAll()).thenReturn(vets);
        when(specialty.canCure(petType)).thenReturn(true);

        clinicService.visitOwnerPets(owner);
        Mockito.verify(visitRepository, times(0)).save(any(Visit.class));
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    // a: age > 3
    // b: daysFromLastVisit > 364
    // c: age <= 3
    // d: daysFromLastVisit > 182
    @UniqueTruePoint(
        predicate = "(a && b) || (c && d)",
        cnf = "(a && b) || (c && d)",
        implicant = "cd",
        valuations = {
            @Valuation(clause = 'a', valuation = false),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = true)
        }
    )

    // a: age > 3
    // b: daysFromLastVisit > 364
    // c: age <= 3
    // d: daysFromLastVisit > 182
    @CACC(
        predicate = "(a && b) || (c && d)",
        majorClause = 'd', //redundant with c being major clause
        valuations = {
            @Valuation(clause = 'a', valuation = false),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = true)
        }, predicateValue = true
    )

    @Test
    public void visitOwnerPetsEnterLoopWithFirstConditionalTrueAndSecondConditionalTrueWithSecondClause() {
        Pet pet = Mockito.mock(Pet.class);

        PetType petType = new PetType();
        petType.setName("mamal");

        java.util.Date birthDate = java.sql.Date.valueOf(LocalDate.now().minusYears(2));
        java.util.Date visitDate = java.sql.Date.valueOf(LocalDate.now().minusDays(183));

        pets.add(pet);

        Owner owner = new Owner();

        Visit last = new Visit();
        last.setDate(visitDate);

        Vet vet = new Vet();
        vet.addSpecialty(specialty);

        vets.add(vet);

        when(pet.getLastVisit()).thenReturn(Optional.of(last));
        when(pet.getType()).thenReturn(petType);
        when(pet.getBirthDate()).thenReturn(birthDate);
        when(petRepository.findByOwner(owner)).thenReturn(pets);
        when(specialty.canCure(petType)).thenReturn(true);
        when(vetRepository.findAll()).thenReturn(vets);

        clinicService.visitOwnerPets(owner);
        Mockito.verify(visitRepository).save(any(Visit.class));
    }
}
