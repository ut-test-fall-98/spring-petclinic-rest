package org.springframework.samples.petclinic.service.userService;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.repository.test.UserRepositoryStubImpl;
import org.springframework.samples.petclinic.service.UserService;
import org.springframework.samples.petclinic.util.UserConfig;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;
import java.util.HashSet;


@RunWith(SpringRunner.class)
@Service
@ContextConfiguration(classes={UserConfig.class}, loader=AnnotationConfigContextLoader.class)
@ActiveProfiles("test")
public class UserServiceImplStateTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private User user;

    @Spy
    private Role role;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Test
    public void saveUserWithNullRoleShouldThrowExceptionTest() throws Exception {
        when(user.getRoles()).thenReturn(null);
        expectedException.expect(Exception.class);
        expectedException.expectMessage("User must have at least a role set!");

        userService.saveUser(user);
    }

    @Test
    public void saveUserWithEmptyRoleShouldThrowExceptionTest() throws Exception {
        when(user.getRoles()).thenReturn(new HashSet<>());

        expectedException.expect(Exception.class);
        expectedException.expectMessage("User must have at least a role set!");

        userService.saveUser(user);
    }

    @Test
    public void saveUserWithRoleThatNameNotStartedWithROLE_PrefixSetNameToAddROLE_PrefixTest() throws Exception {
        String roleName = "ADMIN";
        role.setName(roleName);

        HashSet<Role> hashSet = new HashSet<>();
        hashSet.add(role);

        when(user.getRoles()).thenReturn(hashSet);

        userService.saveUser(user);

        assertEquals("ROLE_" + roleName, role.getName());
    }

    @Test
    public void saveUserWithRoleThatUserIsNullShouldSetUserTest() throws Exception {
        role.setUser(null);
        when(role.getName()).thenReturn("Some Random String");

        HashSet<Role> hashSet = new HashSet<>();
        hashSet.add(role);

        when(user.getRoles()).thenReturn(hashSet);

        userService.saveUser(user);
        assertEquals(user, role.getUser());
    }

    @Test
    public void saveUserShouldIncrementNumberOfUsersTest() throws Exception {
        HashSet<Role> hashSet = new HashSet<>();
        hashSet.add(role);
        when(role.getName()).thenReturn("Some Random String");
        when(user.getRoles()).thenReturn(hashSet);

        int currentUsers = ((UserRepositoryStubImpl)userRepository).getUsersSize();

        userService.saveUser(user);

        assertEquals(currentUsers + 1, ((UserRepositoryStubImpl)userRepository).getUsersSize());
    }
}
