package org.springframework.samples.petclinic.service.userService;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@Service
public class UserServiceImplBehaviorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Captor
    ArgumentCaptor<String> argCaptor;

    @Mock
    private Role role;

    @Mock
    private User user;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void saveUserWithEmptyRoleShouldThrowExceptionTest() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("User must have at least a role set!");

        when(user.getRoles()).thenReturn(new HashSet<>());

        userService.saveUser(user);

        verify(user, times(2)).getRoles();
        verify(userRepository, times(0)).save(user);
    }

    @Test
    public void saveUserWithNullRoleShouldThrowExceptionTest() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("User must have at least a role set!");

        when(user.getRoles()).thenReturn(new HashSet<>());

        userService.saveUser(user);

        verify(user, times(1)).getRoles();
        verify(userRepository, times(0)).save(user);
    }

    @Test
    public void saveUserWithRoleThatNameNotStartedWithROLE_PrefixSetNameToAddROLE_PrefixAndSaveTest() throws Exception {
        HashSet<Role> hashSet = new HashSet<>();
        hashSet.add(role);

        String roleName = "NOT_STARTED_WITH ROLE_";

        when(role.getName()).thenReturn(roleName);
        when(user.getRoles()).thenReturn(hashSet);

        userService.saveUser(user);

        verify(user, times(3)).getRoles();
        verify(role, times(2)).getName();

        verify(role).setName(argCaptor.capture());
        assertEquals("ROLE_" + roleName, argCaptor.getValue());
        verify(role, times(1)).setName(startsWith("ROLE_"));

        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void saveUserWithRoleThatNameStartedWithROLE_PrefixDontSetNameAndSaveTest() throws Exception {
        HashSet<Role> hashSet = new HashSet<>();
        hashSet.add(role);

        when(role.getName()).thenReturn("ROLE_ STARTED_WITH_THAT");
        when(user.getRoles()).thenReturn(hashSet);

        userService.saveUser(user);

        verify(user, times(3)).getRoles();
        verify(role, times(1)).getName();
        verify(role, times(0)).setName(startsWith("ROLE_"));
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void saveUserWithRoleThatUserIsNullShouldSetUserAndSaveTest() throws Exception {
        HashSet<Role> hashSet = new HashSet<>();
        hashSet.add(role);

        when(role.getUser()).thenReturn(null);
        when(role.getName()).thenReturn("Some Random String");
        when(user.getRoles()).thenReturn(hashSet);

        userService.saveUser(user);

        verify(user, times(3)).getRoles();
        verify(role, times(1)).getUser();
        verify(role, times(1)).setUser(user);
        verify(userRepository, times(1)).save(user);
    }
}
