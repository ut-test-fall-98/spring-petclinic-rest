package org.springframework.samples.petclinic.service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.HashSet;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserServiceImplSaveUserTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private Role role;

    @Mock
    private User user;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void saveUserExceptionTest1() throws Exception {
        // Covering [0, 2] with first condition in if
        when(user.getRoles()).thenReturn(null);

        expectedException.expect(Exception.class);
        expectedException.expectMessage("User must have at least a role set!");

        userService.saveUser(user);
    }

    @Test
    public void saveUserExceptionTest2() throws Exception {
        // Covering [0, 2] with second condition in if
        when(user.getRoles()).thenReturn(new HashSet<>());

        expectedException.expect(Exception.class);
        expectedException.expectMessage("User must have at least a role set!");

        userService.saveUser(user);
    }

    @Test
    public void saveUserTest2() throws Exception {
        expectedException = ExpectedException.none();
        // Covering [0,1,3,4,5,3,7,8]
        HashSet<Role> hashSet = new HashSet<>();
        hashSet.add(role);
        when(role.getName()).thenReturn("WITHOUT _ROLE");
        when(role.getUser()).thenReturn(new User());
        when(user.getRoles()).thenReturn(hashSet);

        userService.saveUser(user);
    }

    @Test
    public void saveUserTest3() throws Exception {
        expectedException = ExpectedException.none();
        // Covering [0,1,3,5,6,3,7,8]
        HashSet<Role> hashSet = new HashSet<>();
        hashSet.add(role);
        when(user.getRoles()).thenReturn(hashSet);
        when(role.getName()).thenReturn("ROLE_ADMIN");
        when(role.getUser()).thenReturn(null);

        userService.saveUser(user);
    }
}
