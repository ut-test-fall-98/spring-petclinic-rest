package org.springframework.samples.petclinic.caching;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class SimpleCacheTest {
    @Spy
    EntityManager entityManager;

    @InjectMocks
    SimpleCache simpleCache;

    @Test
    @Ignore
    public void retrieveWhenNotContainsKeyShouldResultInFindingFromEntityManagerAndPuttingInCacheTest() {
        String str = "I am a test object";
        Class c = str.getClass();

        simpleCache.retrieve(c, str);
        simpleCache.retrieve(c, str);

        verify(entityManager, times(1)).find(c, str);
    }

    @Test
    @Ignore
    public void retrieveThenEvictThenRetrieveMustResultInFindingAgainInEntityManagerTest() {
        String str = "I am a test object";
        Class c = str.getClass();

        simpleCache.retrieve(c, str);
        simpleCache.evict(c, str);
        simpleCache.retrieve(c, str);

        verify(entityManager, times(2)).find(c, str);
    }


}
